pragma solidity ^0.4.4;

contract SealableByBlock {

  event LogSeal(address sealer, uint blockNumber);

  uint public blockSealed;

  function isSealed()
    public
    constant
    returns(bool isIndeed)
  {
    return blockSealed != 0;
  }

  function seal()
    internal
    returns(bool success)
  {
    require(!isSealed());

    blockSealed = block.number;
    LogSeal(msg.sender, block.number);
    return true;
  }

}
