pragma solidity ^0.4.4;

import "./MultiAdministered.sol";
import "./Question.sol";

contract PredictionMarket is MultiAdministered {

  event LogQuestionAdded(address creator, address questionAddress, string question, address trustedSource);
  event LogQuestionCleaned(address cleaner, address questionAddress, uint earnings);
  event LogFeeChanged(address changer, uint newFee);
  event LogMarketDrain(address drainer, uint amount);

  uint public fee;

  mapping(address => uint) public questionIndex;
  address[] public questions;

  function PredictionMarket(uint _fee) {
    fee = _fee;
  }

  function isQuestion(address questionAddress)
    public
    constant
    returns(bool isIndeed)
  {
    if (questions.length == 0) return false;
    return (questions[questionIndex[questionAddress]] == questionAddress);
  }

  function getQuestionsCount()
    public
    constant
    returns(uint count)
  {
    return questions.length;
  }

  function getQuestionAtIndex(uint index)
    public
    constant
    returns(address questionAddress)
  {
    return questions[index];
  }

  function setFee(uint newFee)
    fromAdministrator()
    public
    returns(bool success)
  {
    fee = newFee;
    LogFeeChanged(msg.sender, newFee);
    return true;
  }

  function addQuestion(string newQuestion, address newTrustedSource)
    fromAdministrator()
    public
    returns(address newQuestionAddress)
  {
    require(newTrustedSource != address(0));

    Question trustedQuestion = new Question(newQuestion, newTrustedSource, fee);

    questions.push(trustedQuestion);
    questionIndex[trustedQuestion] = questions.length - 1;

    LogQuestionAdded(msg.sender, trustedQuestion, newQuestion, newTrustedSource);
    return trustedQuestion;
  }

  function cleanupQuestion(address questionAddress)
    fromAdministrator()
    public
    returns(bool success)
  {
    require(isQuestion(questionAddress));
    Question q = Question(questionAddress);
    require(q.isReadyForCleanup());
    uint earnings = q.balance;
    q.cleanup(); // selfdestruct

    uint indexToDelete = questionIndex[questionAddress];
    address questionToMove = questions[questions.length - 1];
    questions[indexToDelete] = questionToMove;
    questionIndex[questionToMove] = indexToDelete;
    questions.length--;

    LogQuestionCleaned(msg.sender, questionAddress, earnings);
    return true;
  }

  function withdrawMarketBalance()
    fromOwner()
    public
    returns(bool success)
  {
    LogMarketDrain(msg.sender, this.balance);
    msg.sender.transfer(this.balance);
    return true;
  }

}
