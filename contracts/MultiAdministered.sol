pragma solidity ^0.4.4;

import "./Owned.sol";

contract MultiAdministered is Owned {

  mapping(address => bool) public administrators;

  modifier fromAdministrator {
    require(administrators[msg.sender]);
    _;
  }

  function MultiAdministered() {
    administrators[msg.sender] = true;
  }

  function addAdministrator(address newAdmin)
    fromAdministrator()
    public
    returns(bool success)
  {
    administrators[newAdmin] = true;
    return true;
  }

  function removeAdministrator(address oldAdmin)
    fromAdministrator()
    public
    returns(bool success)
  {
    require(oldAdmin != owner);
    administrators[oldAdmin] = false;
    return true;
  }

}
