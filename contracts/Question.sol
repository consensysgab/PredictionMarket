pragma solidity ^0.4.4;

import "./Owned.sol";
import "./GamblerBased.sol";
import "./SealableByBlock.sol";

contract Question is GamblerBased, Owned, SealableByBlock {

  event LogQuestionAdded(address creator, string question, address trustedSource, uint fee);
  event LogQuestionAnswered(address trustedSource, bool successfullyAnswered, bool answer);
  event LogBetAdded(address gambler, uint bet, bool guess);
  event LogGetWinnings(address gambler, uint earnings);

  enum Answers { None, Yes, No, Error }

  string public question;
  Answers public answer;
  uint public totalBetYes;
  uint public totalBetNo;
  uint public fee;
  address public trustedSource;
  uint public div;
  uint public mod;

  function Question(string newQuestion, address newTrustedSource, uint questionFee)
  {
    require(newTrustedSource != address(0));
    question = newQuestion;
    answer = Answers.None;
    fee = questionFee;
    trustedSource = newTrustedSource;
    LogQuestionAdded(msg.sender, newQuestion, newTrustedSource, questionFee);
  }

  function isAnswered()
    public
    constant
    returns(bool isIndeed)
  {
    return answer == Answers.None ? false : true;
  }

  function isReadyForCleanup()
    public
    constant
    returns(bool isIndeed)
  {
    require(isAnswered());
    return getGamblerCount() == 0;
  }

  function isBalanceRecoverable()
    public
    constant
    returns(bool isIndeed)
  {
    require(isGambler(msg.sender));

    if (!isAnswered()) return false;
    if (answer == Answers.Error) return true;
    if (answer == Answers.Yes && getGamblerBet(msg.sender, true) > 0) return true;
    if (answer == Answers.No && getGamblerBet(msg.sender, false) > 0) return true;
    return false;
  }

  function cleanup()
    fromOwner()
    public
  {
    require(isReadyForCleanup());
    selfdestruct(owner);
  }

  function getWinnings()
    public
    returns(bool success)
  {
    require(isBalanceRecoverable());

    uint earnings;
    if (answer == Answers.Yes)
    {
      earnings = getGamblerBet(msg.sender, true) * div
               + getGamblerBet(msg.sender, true) * mod / totalBetYes;
    }
    else if (answer == Answers.No)
    {
      earnings = getGamblerBet(msg.sender, false) * div
               + getGamblerBet(msg.sender, false) * mod / totalBetNo;
    }
    else if (answer == Answers.Error)
    {
      earnings = ( getGamblerBet(msg.sender, true)
                 + getGamblerBet(msg.sender, false)
                 + getGamblerTimesGambled(msg.sender) * fee );
    }
    deleteGambler(msg.sender);
    msg.sender.transfer(earnings);
    LogGetWinnings(msg.sender, earnings);
    return true;
  }

  function bet(bool guess)
    public
    payable
    returns(bool success)
  {
    require(!isSealed());
    require(msg.value > fee);

    // Time to take our cut
    uint finalBet = msg.value - fee;

    if (guess)
      totalBetYes += finalBet;
    else
      totalBetNo += finalBet;

    if (!isGambler(msg.sender))
      addGambler(msg.sender);
    addBet(msg.sender, guess, finalBet);
    LogBetAdded(msg.sender, finalBet, guess);
    return true;
  }

  function close()
    fromOwner()
    public
    returns(bool success)
  {
    require(seal());
    return true;
  }

  function answer(bool trustedAnswer)
    public
    returns(bool success)
  {
    require(!isAnswered());
    require(msg.sender == trustedSource);

    if (!isSealed())
    {
      blockSealed = block.number;
      answer = Answers.Error;
      LogQuestionAnswered(msg.sender, false, trustedAnswer);
      return false;
    }
    else if (blockSealed >= block.number)
    {
      answer = Answers.Error;
      LogQuestionAnswered(msg.sender, false, trustedAnswer);
      return false;
    }
    else
    {
      if (trustedAnswer)
      {
        answer = Answers.Yes;
        div = (totalBetYes + totalBetNo) / totalBetYes;
        mod = (totalBetYes + totalBetNo) % totalBetYes;
      }
      else
      {
        answer = Answers.No;
        div = (totalBetYes + totalBetNo) / totalBetNo;
        mod = (totalBetYes + totalBetNo) % totalBetNo;
      }

      LogQuestionAnswered(msg.sender, true, trustedAnswer);
      return true;
    }
  }

}
