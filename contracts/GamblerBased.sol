pragma solidity ^0.4.6;

// https://bitbucket.org/rhitchens2/soliditycrud/raw/83703dcaf4d0c4b0d6adc0377455c4f257aa29a7/contracts/SolidityCRUD-part2.sol

contract GamblerBased {

  struct GamblerStruct {
    uint timesGambled;
    uint betYes;
    uint betNo;
    uint index;
  }

  mapping(address => GamblerStruct) private gamblers;
  address[] private gamblerIndex;

  event LogGamblerAdded   (address indexed gamblerAddress, uint index);
  event LogGamblerDeleted (address indexed gamblerAddress, uint index);
  event LogBetAdded       (address indexed gamblerAddress, bool guess, uint bet, uint index);

  function isGambler(address gamblerAddress)
    public
    constant
    returns(bool isIndeed)
  {
    if (gamblerIndex.length == 0) return false;
    return (gamblerIndex[gamblers[gamblerAddress].index] == gamblerAddress);
  }

  function hasBet(address gamblerAddress)
    public
    constant
    returns(bool hasIndeed)
  {
    require(isGambler(gamblerAddress));
    return gamblers[gamblerAddress].betYes > 0
           || gamblers[gamblerAddress].betNo > 0;
  }

  function getGamblerBet(address gamblerAddress, bool guess)
    public
    constant
    returns(uint bet)
  {
    if (!isGambler(gamblerAddress)) return 0;
    if (guess)
      return gamblers[gamblerAddress].betYes;
    else
      return gamblers[gamblerAddress].betNo;
  }

  function getGamblerTimesGambled(address gamblerAddress)
    public
    constant
    returns(uint bet)
  {
    require(isGambler(gamblerAddress));
    return gamblers[gamblerAddress].timesGambled;
  }

  function getGambler(address gamblerAddress)
    public
    constant
    returns(uint timesGambled, uint betYes, uint betNo, uint index)
  {
    require(isGambler(gamblerAddress));
    return(
      gamblers[gamblerAddress].timesGambled,
      gamblers[gamblerAddress].betYes,
      gamblers[gamblerAddress].betNo,
      gamblers[gamblerAddress].index);
  }

  function getGamblerCount()
    public
    constant
    returns(uint count)
  {
    return gamblerIndex.length;
  }

  function getGamblerAtIndex(uint index)
    public
    constant
    returns(address gamblerAddress)
  {
    return gamblerIndex[index];
  }

  function addGambler(address gamblerAddress)
    internal
    returns(uint addedIndex)
  {
    require(!isGambler(gamblerAddress));

    gamblers[gamblerAddress].index = gamblerIndex.push(gamblerAddress)-1;
    LogGamblerAdded(
        gamblerAddress,
        gamblerIndex.length-1);
    return gamblerIndex.length-1;
  }

  function deleteGambler(address gamblerAddress)
    internal
    returns(uint deletedIndex)
  {
    require(!hasBet(gamblerAddress));

    uint rowToDelete = gamblers[gamblerAddress].index;
    address keyToMove = gamblerIndex[gamblerIndex.length-1];
    gamblerIndex[rowToDelete] = keyToMove;
    gamblers[keyToMove].index = rowToDelete;
    gamblerIndex.length--;
    LogGamblerDeleted(
        gamblerAddress,
        rowToDelete);
    return rowToDelete;
  }

  function addBet(address gamblerAddress, bool guess, uint bet)
    internal
    returns(bool success)
  {
    require(isGambler(gamblerAddress));

    gamblers[gamblerAddress].timesGambled += 1;
    if (guess)
      gamblers[gamblerAddress].betYes += bet;
    else
      gamblers[gamblerAddress].betNo += bet;

    LogBetAdded(
      gamblerAddress,
      guess,
      bet,
      gamblers[gamblerAddress].index);
    return true;
  }

}
