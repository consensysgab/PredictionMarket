var MultiAdministered = artifacts.require("./MultiAdministered.sol");

Extensions = require("../utils/extensions.js");
Extensions.init(web3, assert);

contract('MultiAdministered', function(accounts) {
  var contract;
  var owner, admin1, admin2;

  before("should prepare accounts", function () {
    assert.isAbove(accounts.length, 2, "should have at least 3 accounts");
    owner = accounts[0];
    admin1 = accounts[1];
    admin2 = accounts[2];
    return Extensions.makeSureAreUnlocked([ owner ])
      .then(() => web3.version.getNodePromise())
      .then(node => isTestRPC = node.indexOf("EthereumJS TestRPC") > -1)
      .then(() => web3.eth.getBalancePromise(owner))
      .then(balance => assert.isTrue(
        web3.toWei(web3.toBigNumber(10), "finney").lessThan(balance),
        "should have at least 10 finney, not " + web3.fromWei(balance, "finney")));
  });

  beforeEach(function() {
    return MultiAdministered.new({from: owner})
    .then(function(instance){
      contract = instance;
    });
  });

  it("should have owner as adminstrator", function() {
    return contract.owner()
    .then(function(o) {
      assert.equal(owner, o, "Owner is not owned by the right person");
      return contract.administrators(owner);
    })
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
    });
  });

  it("should be able to add an admin", function() {
    return contract.administrators(owner)
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return contract.addAdministrator(admin1);
    })
    .then(function(tx) {
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(true, b, "Admin1 isn't admin");
    });
  });

  it("should't be able to add an admin", function() {
    return contract.administrators(owner)
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return Extensions.expectedExceptionPromise(() => contract.addAdministrator(admin1, {from: admin1, gas: 300000}), 300000);
    })
    .then(function(e) {
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
    });
  });

  it("should be able to add another admin", function() {
    return contract.administrators(owner)
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return contract.addAdministrator(admin1);
    })
    .then(function(tx) {
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(true, b, "Admin1 isn't admin");
      return contract.addAdministrator(admin2, {from: admin1});
    })
    .then(function(tx) {
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(true, b, "Admin2 isn't admin");
    });
  });

  it("should be able to remove admin", function() {
    return contract.administrators(owner)
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return contract.addAdministrator(admin1);
    })
    .then(function(tx) {
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(true, b, "Admin1 isn't admin");
      return contract.addAdministrator(admin2, {from: admin1});
    })
    .then(function(tx) {
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(true, b, "Admin2 isn't admin");
      return contract.removeAdministrator(admin2, {from: admin1});
    })
    .then(function(tx) {
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return contract.removeAdministrator(admin1, {from: admin1});
    })
    .then(function(tx) {
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
    });
  });

  it("should't be able to remove owner as an admin", function() {
    return contract.administrators(owner)
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
      return contract.administrators(admin1);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin1 is admin");
      return contract.administrators(admin2);
    })
    .then(function(b) {
      assert.equal(false, b, "Admin2 is admin");
      return Extensions.expectedExceptionPromise(() => contract.removeAdministrator(owner, {gas: 300000}), 300000);
    })
    .then(function(e) {
      return contract.administrators(owner);
    })
    .then(function(b) {
      assert.equal(true, b, "Owner isn't admin");
    });
  });

});
