var PredictionMarket = artifacts.require("./PredictionMarket.sol");
var Question = artifacts.require("./Question.sol");

Extensions = require("../utils/extensions.js");
Extensions.init(web3, assert);

contract('PredictionMarket', function(accounts) {
  var contract;
  var owner, trustedSource;
  var fee = web3.toWei(web3.toBigNumber(10), "finney");

  before("should prepare accounts", function () {
    assert.isAbove(accounts.length, 1, "should have at least 2 accounts");
    owner = accounts[0];
    trustedSource = accounts[1];
    return Extensions.makeSureAreUnlocked([ owner ])
      .then(() => web3.version.getNodePromise())
      .then(node => isTestRPC = node.indexOf("EthereumJS TestRPC") > -1)
      .then(() => web3.eth.getBalancePromise(owner))
      .then(balance => assert.isTrue(
        web3.toWei(web3.toBigNumber(10), "finney").lessThan(balance),
        "should have at least 10 finney, not " + web3.fromWei(balance, "finney")));
  });

  beforeEach(function() {
    return PredictionMarket.new(fee, {from: owner})
    .then(instance => {
      contract = instance;
    });
  });

//  it("should not be possible to deploy PredictionMarket with value", function() {
//    return Extensions.expectedExceptionPromise(
//      () => PredictionMarket.new(fee, {from: owner, value: 1, gas: 3000000}),
//      3000000);
//  });

  it("should create a question", function() {
    var qTxt = "txt";
    var q1;
    var fullQ;
    return contract.getQuestionsCount.call()
    .then(count => {
      assert.equal(0, count.toString(10), "Count not empty");
      return contract.addQuestion(qTxt, trustedSource);
    })
    .then(tx => {
      q1 = tx.logs[0].args.questionAddress;
      assert.equal(owner, tx.logs[0].args.creator, "Creator is wrong");
      assert.equal(trustedSource, tx.logs[0].args.trustedSource, "TrustedSource is wrong");
      assert.equal(qTxt, tx.logs[0].args.question, "Question is wrong");
      return contract.getQuestionsCount.call();
    })
    .then(count => {
      assert.equal(1, count.toString(10), "Count not one");
      return contract.getQuestionAtIndex.call(0);
    })
    .then(a => {
      assert.equal(q1, a, "Address is wrong");
      return contract.isQuestion.call(q1);
    })
    .then(b => {
      assert.equal(true, b, "QuestionState is wrong");
      fullQ = Question.at(q1);
      return Promise.all([
        fullQ.trustedSource(),
        fullQ.question(),
        fullQ.fee()
      ]);
    })
    .then(infos => {
      assert.equal(trustedSource, infos[0], "TrustedSource is wrong");
      assert.equal(qTxt, infos[1], "Question text is wrong");
      assert.equal(fee.toString(10), infos[2].toString(10), "Fee is wrong");
    });
  });

});
