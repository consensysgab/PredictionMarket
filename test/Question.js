var Question = artifacts.require("./Question.sol");

Extensions = require("../utils/extensions.js");
Extensions.init(web3, assert);

contract('Question', function(accounts) {
  var contract;
  var owner, trustedSource, gambler1, gambler2, gambler3;
  var fee = 10;
  var questionTxt = "Question txt";

  before("should prepare accounts", function () {
    assert.isAbove(accounts.length, 4, "should have at least 5 accounts");
    owner = accounts[0];
    trustedSource = accounts[1];
    gambler1 = accounts[2];
    gambler2 = accounts[3];
    gambler3 = accounts[4];
    return Extensions.makeSureAreUnlocked([ owner, trustedSource, gambler1, gambler2, gambler3 ])
      .then(() => web3.version.getNodePromise())
      .then(node => isTestRPC = node.indexOf("EthereumJS TestRPC") > -1)
      .then(() => web3.eth.getBalancePromise(owner))
      .then(balance => assert.isTrue(
        web3.toWei(web3.toBigNumber(10), "finney").lessThan(balance),
        "should have at least 10 finney, not " + web3.fromWei(balance, "finney")));
  });

  beforeEach(function() {
    return Question.new(questionTxt, trustedSource, fee, {from: owner})
    .then(function(instance){
      contract = instance;
    });
  });

  it("should have the right basic info", function() {
    return contract.trustedSource()
    .then(function(ts) {
      assert.equal(trustedSource, ts, "TrustedSource is wrong");
      return contract.question();
    })
    .then(function(txt) {
      assert.equal(questionTxt, txt, "Question text is wrong");
      return contract.fee();
    })
    .then(function(f) {
      assert.equal(fee.toString(10), f.toString(10), "Fee is wrong");
    });
  });

  it("should be able to bet", function() {
    return contract.bet(true, {from: gambler1, value:1000})
    .then(function(tx) {
      assert.equal(gambler1, tx.logs[1].args.gambler, "Gambler isn't gambler1.");
      assert.equal(1000-fee.toString(10), tx.logs[1].args.bet.toString(10), "Bet isn't 1000-fee.");
      assert.equal(true, tx.logs[1].args.guess, "Guess isn't true.");
      return contract.getGamblerBet.call(gambler1, false);
    })
    .then(function(bet) {
      assert.equal(0, bet.toString(10), "False bet wrong");
      return contract.getGamblerBet.call(gambler1, true);
    })
    .then(function(bet) {
      assert.equal(1000-fee.toString(10), bet.toString(10), "True bet wrong");
    });
  });

  it("shouldn't be able to bet", function() {
    return Extensions.expectedExceptionPromise(() => contract.bet(true, {from: gambler1, value: 1, gas: 300000}), 300000);
  });

  /* To prove that internal functions can't be called: contract.addBet is not a function.
  it("shouldn't be able to bet", function() {
    return Extensions.expectedExceptionPromise(() => contract.addBet(gambler1, 1000, true, {from: gambler1, gas: 300000}), 300000);
  }); */

});
