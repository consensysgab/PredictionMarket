require("file-loader?name=../index.html!../index.html");

const Web3 = require("web3");
const Promise = require("bluebird");
const truffleContract = require("truffle-contract");
const $ = require("jquery");
// Not to forget our built contract
const predictionMarketJson = require("../../build/contracts/PredictionMarket.json");
const questionJson = require("../../build/contracts/Question.json");

// Supports Mist, and other wallets that provide 'web3'.
if (typeof window.web3 !== 'undefined') {
    // Use the Mist/wallet/Metamask provider.
    window.web3 = new Web3(web3.currentProvider);
} else {
    // Your preferred fallback.
    window.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
		// Could use IPC :
		// web3 = new Web3(new Web3.providers.IpcProvider(process.env['HOME'] + '/.ethereum/net42/geth.ipc', require('net')));
}

Promise.promisifyAll(window.web3.eth, { suffix: "Promise" });
Promise.promisifyAll(window.web3.version, { suffix: "Promise" });

const PredictionMarket = truffleContract(predictionMarketJson);
PredictionMarket.setProvider(window.web3.currentProvider);
const Question = truffleContract(questionJson);
Question.setProvider(window.web3.currentProvider);

window.addEventListener('load', function() {
    $("#selectAccount").click(selectAccount);
    $("#newQuestion").click(newQuestion);
    return window.web3.eth.getAccountsPromise()
        .then(accounts => {
            if (accounts.length == 0) {
                $("#balance").html("N/A");
                throw new Error("No account with which to transact");
            }
            window.account = accounts[0];
            $("#accounts").empty();
            var optionStr = ""
            for (i=0; i<accounts.length; i++)
              optionStr += "<option value='"+accounts[i]+"'>"+accounts[i]+"</option>";
            $("#accounts").html(optionStr);
            return PredictionMarket.deployed();
        })
        .then(instance => {
            contract = instance;
            return contract.fee();
        })
        .then(fee => {
            $("#predictionMarketFee").html(fee.toString(10));
            $("#predictionMarketAddress").html(contract.address);
            addSomeEventListener();
            return refreshAccount();
        })
        .then(success => {
            return refreshQuestions();
        })
        .then(success => {
            $("#status").html("Done.");
        })
        .catch(console.error);
});

const addSomeEventListener = function() {
  window.newQEvent = contract.LogQuestionAdded();
  //window.newQEvent.stopWatching();
  window.newQEvent.watch(function(err, result) {
    if (err) {
      console.log(err);
      return
    }
    $("#status").html("New question information coming in...");
    console.log("New Q : "+result.args.creator+" "+result.args.question+" "+result.args.questionAddress+" "+result.args.trustedSource);
    return refreshQuestions()
    .then(success => {
      $("#status").html("Done.");
    })
    .catch(console.error);
  });
};

const refreshAccount = function() {
  $("#account").html(window.account);
  return window.web3.eth.getBalancePromise(window.account)
  .then(balance => {
    $("#balance").html(balance.toString(10))
    return true;
  });
};

const refreshQuestions = function() {
  var questionsTable = "<tr>"
                     + "<th>Question</th>"
                     + "<th>Address</th>"
                     + "<th>Trusted Source</th>"
                     + "<th>Bet</th>"
                     + "<th>My Bet Yes</th>"
                     + "<th>Total Bet Yes</th>"
                     + "<th>Bet Yes</th>"
                     + "<th>My Bet No</th>"
                     + "<th>Total Bet No</th>"
                     + "<th>Bet No</th>"
                     + "</tr>";
  return contract.getQuestionsCount()
  .then(count => {
    return Promise.all(Array.from(Array(parseInt(count)).keys()).map(x => contract.getQuestionAtIndex(x)));
  })
  .then(infos => {
    return Promise.all(
             infos.map(addr =>
               {
                 var q = Question.at(addr);
                 return Promise.all([
                     q.question(),
                     q.trustedSource(),
                     q.totalBetYes(),
                     q.totalBetNo(),
                     q.getGamblerBet(window.account, true),
                     q.getGamblerBet(window.account, false)
                 ])
                 .then(qinfos => {
                   return [
                    qinfos[0],
                    addr,
                    qinfos[1],
                    qinfos[2],
                    qinfos[3],
                    qinfos[4], 
                    qinfos[5]
                   ];
                 });
               }
             )
           );
  })
  .then(infos => {
    infos.map(info =>
        {
          questionsTable += "<tr>"
                          + "<td>"+info[0]+"</td>"
                          + "<td>"+info[1]+"</td>"
                          + "<td>"+info[2]+"</td>"
                          + "<td><input type=\"text\" id="+info[1]+"-bet placeholder=0 size=10 /></td>"
                          + "<td>"+info[5]+"</td>"
                          + "<td>"+info[3]+"</td>"
                          + "<td><button id="+info[1]+"-buttonYes>Bet Yes</button></td>"
                          + "<td>"+info[6]+"</td>"
                          + "<td>"+info[4]+"</td>"
                          + "<td><button id="+info[1]+"-buttonNo>Bet No</button></td>"
                          + "</tr>";
        }
        );
    $("#questionTable").empty();
    $("#questionTable").html(questionsTable);
    infos.map(info =>
        {
          $("#"+info[1]+"-buttonYes").click(function() {bet(info[1], true)});
          $("#"+info[1]+"-buttonNo").click(function() {bet(info[1], false)});
        }
        );
    return true;
  });
};

const bet = function(addr, guess) {
  var amount = $("#"+addr+"-bet")[0].value;
  var q = Question.at(addr);
  $("#status").html("Betting!!");
  q.bet(guess, {from: window.account, value: amount, gas: 200000})
  .then(success => {
    return refreshQuestions();
  })
  .then(success => {
    $("#status").html("Done.");
  })
 .catch(console.error);
};

const selectAccount = function() {
  $("#status").html("Changing account...");
  window.account = $("#accounts")[0].options[$("#accounts")[0].selectedIndex].value;
  return refreshAccount()
  .then(success => {
    return refreshQuestions();
  })
  .then(success => {
    $("#status").html("Done.");
  })
 .catch(console.error);
};

const newQuestion = function() {
  $("#status").html("Sending new question...");
  var q = $("#question")[0].value;
  var ts = $("#trustedSource")[0].value;
  return contract.addQuestion(q, ts, {from: window.account, gas: 1500000})
  .then(tx => {
    return refreshAccount();
  })
  .then(success => {
      $("#status").html("Done.");
  })
  .catch(console.error);
};
